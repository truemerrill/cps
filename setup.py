#!/usr/bin/env python
#
# Composite Pulse Sequencer (CPS)
#
# Copyright (C) 2014 - True Merrill <true.merrill@gtri.gatech.edu>
# Georgia Tech Research Institute
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

from distutils.core import setup

setup(
    name='cps',
    version='0.1',
    description='Composite pulse sequencer utility',
    author='True Merrill',
    author_email='true.merrill@gtri.gatech.edu',
    url='https://bitbucket.org/truemerrill/cps',
    py_modules=['cps'],
    scripts=['cps']
)
