Composite Pulse Sequencer (CPS)
===============================

A handy command-line utility to generate compensating composite pulse sequences
for quantum computing experiments.  A compensating pulse sequence is an ordered
sequence of primitive operations that collectively reduce sensitivity to systematic errors.  Currently, only single-qubit compensating sequences are supported.

Examples
--------
    >> cps --sequence=BB1 3.14159265 0
    Pulse sequence: BB1
    Angle		Phase
    -----		-----
    3.14159265	0.00000000
    3.14159265	1.82347658
    6.28318531	5.47042974
    3.14159265	1.82347658

	>> cps --sequence=CORPSE 1.570796325 1.570796325
    Pulse sequence: CORPSE
    Angle		Phase
    -----		-----
    6.70721635	1.57079633
    5.56045106	4.71238898
    0.42403104	1.57079633

Supported sequences
-------------------
*  B2j family (BB1, B2, B4, B6, B8)
*  N2j family (NB1, N2, N4, N6, N8)
*  PD4, PD6, BB4, BB6
*  CORPSE
*  reducedCinBB
*  reducedCinSK
*  reducedSKinsC

TODO
----
*  Add support for Low Yoder Chuang sequences
*  Add support for Vitanov sequences
*  Add support for multi-qubit compensating sequences