.. cps documentation master file, created by
   sphinx-quickstart on Sat May 31 20:03:19 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cps's documentation!
===============================

Contents:

.. toctree::
   :maxdepth: 2

API documentation
=================
.. automodule:: cps
   :members:
   :undoc-members:

Indices and tables
==================

* :ref:`search`

