#!/usr/bin/python
#
# Composite Pulse Sequencer (CPS)
#
# Copyright (C) 2014 - True Merrill <true.merrill@gtri.gatech.edu>
# Georgia Tech Research Institute
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

"""
.. module:: cps
   :platform: Unix, Linux, Windows
   :synopsis: utility for generating composite pulse sequences

.. moduleauthor:: True Merrill <true.merrill@gtri.gatech.edu>
"""

import numpy
import argparse

from numpy import (pi, sin, cos, arcsin, arccos, mod, matrix, real, mean)
from numpy.linalg import (eig)
from scipy import (sqrt, log)

# Pauli operators
I = matrix("1,0;0,1")
X = matrix("0,1;1,0")
Y = matrix("0,-1j;1j,0")
Z = matrix("1,0;0,-1")


# -----------------------------------------------------------------------------
# Functions to produce simple rotations
#

def generator(phi, f=0):
    r"""Function returning the pulse generator
    :math:`\sigma_\phi = X \cos \phi + Y \sin \phi`, where :math:`\phi` is the
    laser phase.

    :param phi: laser phase
    :type phi: ``float``
    :param f: fractional pulse detuning (in units of Rabi frequency)
    :type f: ``float``
    """
    return X * cos(phi) + Y * sin(phi) + Z * f


def rotate(theta, phi, f=0):
    r"""Function returning the propagator for a simple rotation
    :math:`R(\theta, \phi) = \exp(- i \theta \sigma_\phi / 2)`.

    :param theta: rotation angle
    :type theta: ``float``
    :param phi: laser phase
    :type phi: ``float``
    :param f: fractional pulse detuning (in units of Rabi frequency)
    :type f: ``float``
    """
    # If the rotation angle is negative, advance the phase by pi and use a
    # positive rotation angle
    if theta < 0:
        theta = -theta
        phi += pi

    # Account for fractional off-resonance
    theta = theta * sqrt(1. + f ** 2)
    sigma = generator(phi, f) / sqrt(1. + f ** 2)
    U = cos(theta / 2.) * I - 1j * sin(theta / 2.) * sigma
    return U


# -----------------------------------------------------------------------------
# Class to parameterize pulse sequences
#

class Sequence(numpy.ndarray):
    """Object parameterizing a composite pulse sequence.  A :class:`Sequence`
    object is a `(n, 2)` dimensional array.  Each row contains two entries, an
    angle, and a phase which parameterize a single simple rotation.  Each
    successive row represents a new pulse in the sequence, ordered from the top
    to bottom.
    """
    detuning = 0

    def __new__(cls, input_array):
        obj = numpy.asarray(input_array).view(cls)
        return obj

    def __array_finalize__(self, obj):
        return None

    def __mul__(self, target):
        if not isinstance(target, Sequence):
            message = 'Must multiply a sequence object by another sequence'
            raise TypeError(message)

        # Multiply two sequences by creating a new sequence object with pulses
        # from both sequences.  The target goes first since the time-ordering
        # goes from top to bottom.
        if target.shape == (0,):
            return self

        elif self.shape == (0,):
            return target

        else:
            seq = numpy.vstack((target, self))
            return Sequence(seq)

    def __pow__(self, n):
        seq = Sequence([])
        for j in range(n):  # @UnusedVariable
            seq = seq * self
        return seq

    def clean(self):
        """Method to `clean` the pulse sequence by requiring each rotation
        angle to be positive and the phase to be less than 2 * pi.
        """
        for pulse in self:
            theta = pulse[0]
            phi = pulse[1]

            # Check for negative rotation angles.  If angles are
            # negative, apply a positive rotation about the opposite.
            if theta < 0:
                theta = -theta
                phi = phi + pi

            # Check that phi is in (0, 2pi)
            phi = mod(phi, 2 * pi)

            # Update stored values
            pulse[0] = theta
            pulse[1] = phi

    def operator(self):
        """Method returning a representation of the net sequence propagator"""
        U = I
        for pulse in self:
            U = rotate(pulse[0], pulse[1], f=self.detuning) * U
        return U

    def table(self):
        """Method returning a table of sequence parameters"""
        string = '%s\t\t%s\n' % ('Angle', 'Phase')
        string += '%s\t\t%s\n' % ('-----', '-----')
        for pulse in self:
            string += '%1.8f\t%1.8f\n' % (pulse[0], pulse[1])
        return string


# -----------------------------------------------------------------------------
# Methods to simulate systematic errors
#

def infidelity(U, V):
    """Function to calculate the infidelity between two unitary matrices.  The
    infidelity is equivalent to :math:`1 - |tr(U^\dagger V)|/2` for two
    dimensional matrices.

    :param U: first matrix
    :type U: :class:`numpy.matrix`
    :param V: second matrix
    :type V: :class:`numpy.matrix`
    """
    [q, Q] = eig(U.H * V)  # @UnusedVariable
    f = real(1j * log(q))
    f = f - mean(f)
    dst = 2 * max(sin(f / 2.))
    infd = dst ** 2 / 2.
    if infd > 1:
        infd = 2. - infd
    return infd


def amplitude_error(sequence, error):
    """Function to apply an amplitude error with amplitude `error` to
    `sequence`.  An amplitude error scales the the length of each rotation
    angle in the sequence by `(1 + error)`.

    :param sequence: input (error-free) pulse sequence
    :type sequence: :class:`Sequence`
    :param error: error parameter
    :type error: ``float``
    """
    error_sequence = sequence.copy()
    for pulse in error_sequence:
        pulse[0] = pulse[0] * (1 + error)
    return error_sequence


def addressing_error(sequence, error):
    """Function to apply an addressing error with amplitude `error` to
    `sequence`.  An addressing error scales the the length of each rotation
    angle in the sequence by `(error)`.

    :param sequence: input (error-free) pulse sequence
    :type sequence: :class:`Sequence`
    :param error: error parameter
    :type error: ``float``
    """
    error_sequence = sequence.copy()
    for pulse in error_sequence:
        pulse[0] = pulse[0] * error
    return error_sequence


def detuning_error(sequence, error):
    """Function to apply a detuning error with amplitude `error` to
    `sequence`.  A detuning error adds an additional `Z` portion to each pulse
    Hamiltonian the the length of each rotation
    angle in the sequence by `(1 + error)`.

    :param sequence: input (error-free) pulse sequence
    :type sequence: :class:`Sequence`
    :param error: error parameter
    :type error: ``float``
    """
    error_sequence = sequence.copy()
    error_sequence.detuning = error
    return error_sequence


def error_scaling(sequence, error_function):
    """Function to calculate scaling with respect to systematic errors.

    :param sequence: input (error-free) pulse sequence
    :type sequence: :class:`Sequence`
    :param error_function: function that applies error to `sequence`
    :type error_function: callable
    """
    # Create a grid of systematic error values
    log_error = numpy.linspace(-3., 0., 100)
    error = numpy.power(10, log_error)

    # Sample the sequence fidelity over the grid of error values
    target = error_function(sequence, 0).operator()
    infidelities = numpy.zeros(error.shape)
    for index, err in enumerate(error):
        error_sequence = error_function(sequence, err)
        gate = error_sequence.operator()
        infidelities[index] = infidelity(target, gate)

    return error, infidelities

_registered_errors = {
    'amplitude': amplitude_error,
    'addressing': addressing_error,
    'detuning': detuning_error
}


# -----------------------------------------------------------------------------
# Constructors for specific pulse sequences
#

def pulse(theta, phi):
    """Constructor function for a simple pulse.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return Sequence([[theta, phi]])


def N2j(theta, phi, j):
    """Constructor function for the N2j narrowband sequence.  See PRA 70,
    052318 (2004) for formulation.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    :param j: order parameter (the order is 2j)
    :type j: ``int``
    """
    def S(phi1, phi2, m, n):
        """Recursive constructor"""
        if n == 1:
            return pulse(m * pi, phi + phi1) * \
                   pulse(2 * m * pi, phi + phi2) * \
                   pulse(m * pi, phi + phi1)

        elif n > 1:
            return S(phi1, phi2, m, n - 1) ** (4 ** (n - 1)) * \
                   S(phi1, phi2, -2 * m, n - 1) * \
                   S(phi1, phi2, m, n - 1) ** (4 ** (n - 1))

    def fj(j):
        """Function used to compute the sequence phases"""
        if j == 1:
            return 1.
        else:
            return (2. ** (2. * j - 1.) - 2) * fj(j - 1)

    phij = arccos(-theta / (4 * pi * fj(j)))
    return S(phij, -phij, 1, j) * pulse(theta, phi)


def B2j(theta, phi, j):
    """Constructor function for the B2j broadband sequence.  See PRA 70,
    052318 (2004) for formulation.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    :param j: order parameter (the order is 2j)
    :type j: ``int``
    """
    def S(phi1, phi2, m, n):
        # Constructor notation, used in PRA 70,052318 (2004)
        if n == 1:
            return pulse(m / 2. * pi, phi + phi1) * \
                   pulse(m * pi, phi - phi1 + 4 * phi1 * ((m / 2.) % 2)) * \
                   pulse(m / 2. * pi, phi + phi1)
        elif n > 1:
            return S(phi1, phi2, m, n - 1) ** (4 ** (n - 1)) * \
                   S(phi1, phi2, -2 * m, n - 1) * \
                   S(phi1, phi2, m, n - 1) ** (4 ** (n - 1))

    def fj(j):
        if j == 1:
            return 1
        else:
            return (2 ** (2 * j - 1) - 2) * fj(j - 1)

    phib = arccos(-theta / (4 * pi * fj(j)))
    return S(phib, 3 * phib, 2, j) * pulse(theta, phi)


def N2(theta, phi):
    """Constructor function for the N2 narrowband sequence.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return N2j(theta, phi, 1)


def N4(theta, phi):
    """Constructor function for the N4 narrowband sequence.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return N2j(theta, phi, 2)


def N6(theta, phi):
    """Constructor function for the N6 narrowband sequence.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return N2j(theta, phi, 3)


def N8(theta, phi):
    """Constructor function for the N8 narrowband sequence.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return N2j(theta, phi, 4)


def B2(theta, phi):
    """Constructor function for the B2 broadband sequence.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return B2j(theta, phi, 1)


def B4(theta, phi):
    """Constructor function for the B4 broadband sequence.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return B2j(theta, phi, 2)


def B6(theta, phi):
    """Constructor function for the B6 broadband sequence.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return B2j(theta, phi, 3)


def B8(theta, phi):
    """Constructor function for the B8 broadband sequence.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return B2j(theta, phi, 4)


def BB1(theta, phi):
    """Constructor function for the BB1 broadband sequence.  BB1 is a synonym
    for B2.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return B2(theta, phi)


def NB1(theta, phi):
    """Constructor function for the NB1 narrowband sequence.  NB1 is a synonym
    for N2.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    return N2(theta, phi)


def CORPSE(theta, phi):
    """Constructor function for the CORPSE sequence.  CORPSE corrects detuning
    errors to first-order.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    n1, n2, n3 = (1, 1, 0)

    # Calculate rotation angles
    k = arcsin(sin(theta / 2.) / 2.)
    theta1 = 2 * pi * n1 + theta / 2. - k
    theta2 = 2 * pi * n2 - 2 * k
    theta3 = 2 * pi * n3 + theta / 2. - k

    return pulse(theta3, phi) * \
        pulse(-theta2, phi) * \
        pulse(theta1, phi)


def reducedCinBB(theta, phi):
    """Constructor function for the reduced CinBB sequence.  See Bando et al.
    J. Phys. Soc. Japan, 82 10.7566 (2012) for details.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    # Calculate rotation angles
    k = arcsin(sin(theta / 2.) / 2.)
    s = arccos(-theta / (4 * pi))

    return pulse(pi, phi + s) * \
        pulse(2 * pi, 3 * (phi + s) - 2 * phi) * \
        pulse(pi, phi + s) * \
        pulse(2 * pi + theta / 2. - k, phi) * \
        pulse(2 * pi - 2 * k, phi + pi) * \
        pulse(theta / 2. - k, phi)


#
# Phases from LYC paper
#
# TODO: Replace this with a method to calculate the phases for arbitrary
# rotation angles.
#

PD4_1_phi = numpy.array([
    [2.26950, -1.76948, -0.80579, 1.93044],
    [2.30661, -1.30540, -0.47998, 2.38635],
    [2.45079, -0.96051, -0.28079, 2.6642]
])


PD6_4_phi = numpy.array([
    [0.38266, -3.00356, -2.24117, 2.23067, -1.43621, 0.84607],
    [0.34769, -2.51801, 2.11029, -1.90548, 1.90034, -0.66420],
    [0.61658, -2.23483, 2.05399, -1.700970, 2.20472, -0.51392]
])


def PD4(theta, phi):
    """Constructor function for the PD4-1 sequence.  PD4-1 corrects amplitude
    errors to fourth-order. Only valid for theta = 2pi, pi, and pi/2

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """

    row = round(numpy.log2(2 * pi / theta))
    if row > 2 or row < 0:
        message = "Not a valid angle for this method"
        raise ValueError(message)

    #return pulse(0, 0)
    PD4seq = pulse(theta, phi)
    for l in range(0, 4):
        PD4seq = pulse(2 * pi, PD4_1_phi[row, l] + phi) * PD4seq
    for l in range(0, 4):
        PD4seq = pulse(2 * pi, PD4_1_phi[row, 3 - l] + phi) * PD4seq
    return PD4seq


def BB4(theta, phi):
    """Constructor function for the BB4-1 sequence based on the PD4-1 sequence.
    BB4-1 corrects amplitude errors to fourth-order in a broadband way. Only
    valid for theta = pi, pi/2, and pi/4

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    row = round(numpy.log2(pi / theta))
    if row > 2 or row < 0:
        message = "Not a valid angle for this method"
        raise ValueError(message)

    # generate BB phases from PD phases
    PD4phases = PD4_1_phi[row]
    PD4phases = numpy.append(PD4phases, PD4phases[::-1])
    BB4_phi = numpy.array([])
    for l in range(0, PD4phases.size):
        phi_temp = 0
        for k in range(0, l):
            phi_temp = phi_temp - 1. * (-1) ** (k + 1) * PD4phases[k]

        for k in range(l + 1, PD4phases.size):
            phi_temp = phi_temp + 1. * (-1) ** (k + 1) * PD4phases[k]

        BB4_phi = numpy.append(BB4_phi, phi_temp)

    # generate sequence
    BB4seq = pulse(theta, phi)
    for l in range(0, PD4phases.size):
        BB4seq = pulse(pi, BB4_phi[l] + phi) * BB4seq

    return BB4seq


def PD6(theta, phi):
    """Constructor function for the PD6-4 sequence.  PD6-4 corrects amplitude
    errors to fourth-order. Only valid for theta = 2pi, pi, and pi/2

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    row = round(numpy.log2(2 * pi / theta))
    if row > 2 or row < 0:
        message = "Not a valid angle for this method"
        raise ValueError(message)

    PD6seq = pulse(theta, phi)
    for l in range(0, 6):
        PD6seq = pulse(2 * pi, PD6_4_phi[row, l] + phi) * PD6seq
    for l in range(0, 6):
        PD6seq = pulse(2 * pi, PD6_4_phi[row, 3 - l] + phi) * PD6seq
    return PD6seq


def BB6(theta, phi):
    """Constructor function for the BB6-4 sequence based on the PD6-4 sequence.
    BB6-4 corrects amplitude errors to fourth-order in a broadband way. Only
    valid for theta = pi, pi/2, and pi/4

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    row = round(numpy.log2(pi / theta))
    if row > 2 or row < 0:
        message = "Not a valid angle for this method"
        raise ValueError(message)

    # generate phases
    PD6phases = PD6_4_phi[row]
    PD6phases = numpy.append(PD6phases, PD6phases[::-1])
    BB6_phi = numpy.array([])
    for l in range(0, PD6phases.size):
        phi_temp = 0
        for k in range(0, l):
            phi_temp = phi_temp - 1. * (-1) ** (k + 1) * PD6phases[k]

        for k in range(l + 1, PD6phases.size):
            phi_temp = phi_temp + 1. * (-1) ** (k + 1) * PD6phases[k]
        BB6_phi = numpy.append(BB6_phi, phi_temp)

    # generate sequence
    BB6seq = pulse(theta, phi)
    for l in range(0, PD6phases.size):
        BB6seq = pulse(pi, BB6_phi[l] + phi) * BB6seq

    return BB6seq


def reducedCinSK(theta, phi):
    """Constructor function for the reduced CinSK sequence.  See Bando et al.
    J. Phys. Soc. Japan, 82 10.7566 (2012) for details.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    # Calculate rotation angles
    k = arcsin(sin(theta / 2.) / 2.)
    s = arccos(-theta / (4 * pi))

    return pulse(2 * pi, phi + s) * \
        pulse(2 * pi, phi - s) * \
        pulse(2 * pi + theta / 2. - k, phi) * \
        pulse(2 * pi - 2 * k, phi + pi) * \
        pulse(theta / 2. - k, phi)


def reducedSKinsC(theta, phi):
    """Constructor function for the reduced SKinsC sequence.  See Bando et al.
    J. Phys. Soc. Japan, 82 10.7566 (2012) for details.

    :param theta: rotation angle for the net propagator
    :type theta: ``float``
    :param phi: phase for the net propagator
    :type phi: ``float``
    """
    # Calculate rotation angles
    k = arcsin(sin(theta / 2.) / 2.)
    s = arccos(-theta / (4 * pi))

    return pulse(theta / 2. - k, phi) * \
        pulse(2 * pi - theta / 2. - k, phi + pi) * \
        pulse(2 * pi, phi + pi - s) * \
        pulse(2 * pi, phi + pi + s) * \
        pulse(theta / 2. - k, phi + pi) * \
        pulse(theta / 2. - k, phi)


# -------------------------------------------------------------------------
# Argument parsing for command line tool
#

def _arg_parser():
    """Private method to create a :class:`argparse.ArgumentParser` instance
    used to parse command line arguments
    """
    parser = argparse.ArgumentParser(
        description='Calculate a composite pulse sequence for a ' + \
            'single-qubit gate.  A sequence is an ordered set of ' + \
            'rotations, with each rotation determined by an angle ' + \
            'and phase in the X-Y plane.'
    )
    parser.add_argument(
        'angle',
        metavar='theta',
        type=float,
        nargs=1,
        help='Rotation angle for the pulse sequence net propagator'
    )
    parser.add_argument(
        'phase',
        metavar='phi',
        type=float,
        nargs=1,
        help='Phase for the pulse sequence net propagator'
    )
    parser.add_argument(
        '--sequence',
        action='store',
        dest='sequence',
        default='pulse',
        help='Pulse sequence type [BB1, B2, B4, B6, B8 ...]'
    )
    parser.add_argument(
        '--display-propagator',
        action='store_true',
        default=False,
        help='Display net propagator with output'
    )
    parser.add_argument(
        '--scaling-plot',
        action='store',
        dest='error_type',
        default='None',
        help='Display a scaling plot [amplitude, detuning, ...]'
    )
    return parser


def _main():
    """Private method for the main program"""
    parser = _arg_parser()
    args = parser.parse_args()

    # Check and see if the sequence exists as a function in the local
    # namespace.  If not, print an error message

    if args.sequence in globals():
        func = globals()[args.sequence]
        theta = args.angle[0]
        phi = args.phase[0]
        sequence = func(theta, phi)
        sequence.clean()

        print 'Pulse sequence: %s' % args.sequence
        print sequence.table()

        if args.display_propagator:
            print 'Net pulse propagator:'
            print sequence.operator()

        if not args.error_type == 'None':
            error_type = args.error_type
            if error_type in _registered_errors:

                # Create a scaling plot, comparing the sensitivity of the pulse
                # sequence to a simple pulse.
                error_function = _registered_errors[error_type]
                label = error_type + ' error'
                e0, i0 = error_scaling(pulse(theta, phi), error_function)
                e1, i1 = error_scaling(sequence, error_function)

                try:
                    print 'Drawing error scaling plot ...'
                    import matplotlib.pyplot as plt
                    plt.loglog(e0, i0, 'k--',
                               e1, i1, 'b')
                    plt.xlabel(label.capitalize())
                    plt.ylabel('Infidelity')
                    plt.show()
                except:
                    message = 'matplotlib required for plotting function'
                    print message
            else:
                message = 'No such error type \'%s\'' % (error_type)
                print message

    else:
        message = 'No such sequence \'%s\'' % (args.sequence)
        print message


if __name__ == "__main__":
    # Run the main program
    _main()
