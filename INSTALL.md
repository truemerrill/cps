Installation instructions
=========================

`cps` uses Python `Distutils` to compile and install the relevant Python
package and command-line utility.  To install the tool, checkout the source
code or extract the source tarball and open a terminal in the source directory.
Issue the command

	>> python setup.py install

This will compile and install the package and scripts

	running install
	running build
	running build_py
	creating build
	creating build/lib.linux-x86_64-2.7
	copying cps.py -> build/lib.linux-x86_64-2.7
	running build_scripts
	creating build/scripts-2.7
	copying and adjusting cps -> build/scripts-2.7
	changing mode of build/scripts-2.7/cps from 644 to 755
	running install_lib
	copying build/lib.linux-x86_64-2.7/cps.py -> /usr/local/lib/python2.7/dist-packages
	byte-compiling /usr/local/lib/python2.7/dist-packages/cps.py to cps.pyc
	running install_scripts
	copying build/scripts-2.7/cps -> /usr/local/bin
	changing mode of /usr/local/bin/cps to 755
	running install_egg_info
	Writing /usr/local/lib/python2.7/dist-packages/cps-0.1.egg-info

Installation will require root permissions if the package is installed in the
system-wide `site-packages` directory.  See `Distutils` documentation for
user-level installation instructions.

Upgrading
---------

Upgrading `cps` is equivalent to reinstallation of the package.  Upgrade the
source code by checking out a recent version from the repository or by
extracting a more recent tarball.  Then follow the usual installation
instructions to replace `cps` with a newer version.
    